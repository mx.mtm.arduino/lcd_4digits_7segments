#define NUM_DIGITS 4
#define NUM_DIGIT_PINS 4
#define NUM_SEGMENT_PINS 7

// This will specify which digits are to be left blank
#define EMPTY_VALUE 255

// Button pin
#define BUTTON_PIN A1
// Button state register
byte buttonLastState = 0;
// Time since last button state change (calculated only while pressed)
uint16_t buttonDownTimestamp = 0;
// Time during which the button was pressed last time 
uint32_t buttonDownDuration = 0;

#define TIME_UNTIL_POWER_CYCLE 4 // Time it takes to go into stand-by mode [seconds]
#define TIME_UNTIL_RESET 2 // Time it takes to reset the counter [seconds]

// Power modes 
#define PMODE_NORMAL 0
#define PMODE_SWITCHING 1

#define ON 1
#define OFF 0

byte powerMode = PMODE_NORMAL;
boolean powerState = OFF;

uint16_t counter = 0;

// Digit pins
byte digitPins[NUM_DIGIT_PINS] = {9, 10, 11, 12};
// Segment pins
byte segmentPins[NUM_SEGMENT_PINS] = {2, 3, 4, 5, 6, 7, 8};

// Setup numbers
byte numbers[10][NUM_SEGMENT_PINS] = {
  {1, 1, 1, 1, 1, 1, 0}, // 0
  {0, 0, 1, 0, 0, 1, 0}, // 1
  {1, 0, 1, 1, 1, 0, 1}, // 2
  {1, 0, 1, 0, 1, 1, 1}, // 3
  {0, 1, 1, 0, 0, 1, 1}, // 4
  {1, 1, 0, 0, 1, 1, 1}, // 5
  {1, 1, 0, 1, 1, 1, 1}, // 6
  {1, 0, 1, 0, 0, 1, 0}, // 7
  {1, 1, 1, 1, 1, 1, 1}, // 8
  {1, 1, 1, 0, 1, 1, 1}, // 9  
};

// Stores digits to be displayed
byte displayArray[NUM_DIGITS] = {EMPTY_VALUE, EMPTY_VALUE, EMPTY_VALUE, EMPTY_VALUE};

void setup() {
  byte i;
  // Setup pins for digit selection
  for(i=0; i<NUM_DIGIT_PINS; i++) {
    pinMode(digitPins[i], OUTPUT);
  }
  
  // Setup pins for segment output
  for(i=0; i<NUM_SEGMENT_PINS; i++) {
    pinMode(segmentPins[i], OUTPUT);
  }

  pinMode(BUTTON_PIN, INPUT);
 
  _setValue(counter);
}

void loop() {
  _checkButtonState();
  
  _resetDisplay();
  if(powerState == ON) {
    _displayValue();
  }  
}

/**
* Button processing. Ideally button should take care of powering up/down, resetting and increasing the counter value.
*/
void _checkButtonState() {
  byte buttonState = digitalRead(BUTTON_PIN);

  if( buttonState == HIGH ) { // Button is pressed
    if( buttonLastState != buttonState ) { // This is the start of "pressed" condition, store the time of event
      buttonDownTimestamp = millis();
    } else { // Button is pressed for some time now
      if(powerMode == PMODE_NORMAL) {
        buttonDownDuration = millis() - buttonDownTimestamp;
      
        if(buttonDownDuration >= TIME_UNTIL_POWER_CYCLE*1000) { // Super-looooong press, go to stand-by while the button is still pressed
          if(powerState == ON) {
              powerState = OFF;
          } else {
            powerState = ON;
          }
          
          powerMode = PMODE_SWITCHING;
          buttonDownDuration = buttonDownTimestamp = 0;
          return; 
        } else if(powerState == ON && buttonDownDuration >= TIME_UNTIL_RESET*1000) { // Long press, reset the counter
          counter = 0;
          _setValue(counter);
        }
      }
    }
  } else {
    if( buttonLastState != buttonState ) { // Button is released
      buttonDownDuration = millis() - buttonDownTimestamp;
      if( powerState == ON ) {
        if(buttonDownDuration < TIME_UNTIL_RESET*1000) { // Short press, increase counter value
          counter++;
          _setValue(counter);
        }
        
        buttonDownDuration = buttonDownTimestamp = 0;
      }
      powerMode = PMODE_NORMAL;
    }
  }

  buttonLastState = buttonState;
}


/**
* Demo mode - counting all values on all digit blocks simultaneously
*/
void demoMode() {
  for(byte i=1; i<10; i++) {
    _setValue(i*1111);
    delay(1000);
  }
}


/**
* Set all ground pins (digit selector) to HIGH (write-protection)
*/
void _resetDigitPins() {
  byte pin;
  for(pin=0; pin<NUM_DIGIT_PINS; pin++) {
    digitalWrite(digitPins[pin], HIGH);
  }
}

/**
* Calculates the needed segments and displays the requested number
*/
void _setValue(uint16_t value) {  
  byte digitValue = 0, 
    currentValue = 0,
    digitsToProcess = NUM_DIGITS;
    
  // Reset all digit values first...
  for(byte i=0; i<NUM_DIGITS; i++) {
    displayArray[i] = EMPTY_VALUE;
  }
    
  if(value == 0) {
    displayArray[digitsToProcess-1] = 0;
    return;
  }
  
  // Additional check to avoid possible value overflow
  value = constrain(value, 0, 9999);
  
  // Calculate the required segments
  boolean lastDigit = false;  
  while(digitsToProcess > 0) {
    if(value < 10) { // This will be last digit, set it bail out
      digitValue = value;
      lastDigit = true;
    } else {
      digitValue = (byte)(value % 10);
      value = (uint16_t)(value / 10);
    }
    displayArray[digitsToProcess-1] = digitValue;
    if(lastDigit) {
      break;
    } else {
      digitsToProcess--;
    }
  }
}

void _displayValue() {
  for(byte digit=0; digit<NUM_DIGIT_PINS; digit++) {
    _resetDigitPins();
    if(displayArray[digit] != EMPTY_VALUE) {
      digitalWrite(digitPins[digit], LOW);
      for(byte segment=0; segment<NUM_SEGMENT_PINS; segment++) {
        digitalWrite(segmentPins[segment], numbers[displayArray[digit]][segment]);  
      }
    }
    delay(5);
  }
}

void _resetDisplay() {  
  for(byte digit=0; digit<NUM_DIGIT_PINS; digit++) {
    digitalWrite(digitPins[digit], LOW);
  }
  for(byte segment=0; segment<NUM_SEGMENT_PINS; segment++) {
    digitalWrite(segmentPins[segment], LOW); 
  }
}
